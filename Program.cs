﻿using System;

namespace Tdelegates
{
	class Program
	{
		static void Main(string[] args)
		{
			int adult = 0;
			int a = 0;
			int n = 0;
			Counter counter = new Counter();
			Console.WriteLine("Introdu câte persoane doresti să adaugi:");
			int nr = Convert.ToInt32(Console.ReadLine());
			counter.people = new People[nr];
			for (int i = 0; i < nr; i++)
			{
				Console.Write("Vârstă:");
				int age = Convert.ToInt32(Console.ReadLine());
				if (age >= 18)
					adult++;
				Console.Write("Nume:");
				string name = Console.ReadLine();
				if (name.Contains('a') || name.Contains('A'))
					a++;
				counter.people[i] = new People(age, name, counter);
				Console.WriteLine("------------------");
			}
			Console.WriteLine("Alege un filtru a persoanelor introduse:");
			Console.WriteLine("\n 1-> Doar persoanele care continu caracterul „a”");
			Console.WriteLine("\n 2-> Doar persoanele adulte peste 18 ani");
			Console.WriteLine("Numărul opțiunii: ");
			int choice = Convert.ToInt32(Console.ReadLine());
			Counter counter1 = new Counter();
			switch (choice)
			{
				case 1:
					counter1.people = new People[a];
					for (int i = 0; i < nr; i++)
					{
						if (counter.people[i].Name.Contains('a') || counter.people[i].Name.Contains('A'))
						{
							counter1.people[n] = new People(counter.people[i].Age, counter.people[i].Name, counter1);
							n++;
						}
					}
					nr = a;
					break;
				case 2:
					counter1.people = new People[adult];
					for (int i = 0; i < nr; i++)
					{
						if (counter.people[i].Age >= 18)
						{
							counter1.people[n] = new People(counter.people[i].Age, counter.people[i].Name, counter1);
							n++;
						}
					}
					nr = adult;
					break;
			}
			counter1.Count();
			Console.WriteLine("\n");
			for (int i = 0; i < nr; i++)
			{
				Console.WriteLine("Vârstă:" + counter1.people[i].Age + "    Nume:" + counter1.people[i].Name);
			}
		}
	}

	public delegate void Del();

	public class Counter
	{
		public People[] people;
		public event Del Red;

		public void Count()
		{
			if (people.Length > 5)
				Red();
		}
	}

	public class People
	{
		public int Age;
		public string Name;

		public People(int age, string name, Counter counter)
		{
			Age = age;
			Name = name;
			counter.Red += () => Console.ForegroundColor = ConsoleColor.Red;
		}

	}
}
